package utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import pojo.Well;

public class UtilsTest {

    private static final String NAME = "name";

    @Test
    void shouldGetNeedWellsWithComaDelimiterTest() {
        String input = "name,name2";
        Well well_1 = new Well(1, NAME);
        Well well_2 = new Well(1, NAME+1);
        Well well_3 = new Well(1, NAME+2);
        Well well_4 = new Well(1, NAME+3);

        //--------------------------------------------------------------------------------------------------------------
        List<Well> expectedList = new ArrayList<>();
        expectedList.add(well_1);
        expectedList.add(well_3);

        List<Well> allWells = new ArrayList<>();
        allWells.add(well_1);
        allWells.add(well_2);
        allWells.add(well_3);
        allWells.add(well_4);
        List<Well> needWellsList = Utils.getNeedWellsListFromAll(input, allWells);
        //--------------------------------------------------------------------------------------------------------------
        Assertions.assertEquals(expectedList.size(), needWellsList.size());
        Assertions.assertTrue(needWellsList.contains(well_1));
        Assertions.assertTrue(needWellsList.contains(well_3));
        //--------------------------------------------------------------------------------------------------------------
    }

    @Test
    void shouldGetNeedWellsWithSpaceDelimiterTest() {
        String input = "name3 name1";
        Well well_1 = new Well(1, NAME);
        Well well_2 = new Well(1, NAME+1);
        Well well_3 = new Well(1, NAME+2);
        Well well_4 = new Well(1, NAME+3);

        //--------------------------------------------------------------------------------------------------------------
        List<Well> expectedList = new ArrayList<>();
        expectedList.add(well_4);
        expectedList.add(well_2);

        List<Well> allWells = new ArrayList<>();
        allWells.add(well_1);
        allWells.add(well_2);
        allWells.add(well_3);
        allWells.add(well_4);
        List<Well> needWellsList = Utils.getNeedWellsListFromAll(input, allWells);
        //--------------------------------------------------------------------------------------------------------------
        Assertions.assertEquals(expectedList.size(), needWellsList.size());
        Assertions.assertTrue(needWellsList.contains(well_4));
        Assertions.assertTrue(needWellsList.contains(well_2));
        //--------------------------------------------------------------------------------------------------------------
    }
}
