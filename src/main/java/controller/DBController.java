package controller;

import pojo.Well;

import java.sql.SQLException;
import java.util.List;

public interface DBController {

    void createTables() throws SQLException;
    String createEquipmentAndWellIfNeed(String wellName, int countEquipment);
    List<Well> getNeededWells(String input);
    List<Well> getAllWellsWithEquipments();
    void close() throws SQLException;
}
