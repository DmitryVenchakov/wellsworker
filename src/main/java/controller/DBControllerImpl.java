package controller;

import pojo.Well;
import service.DBService;

import java.sql.SQLException;
import java.util.List;

public class DBControllerImpl implements DBController {

    private static DBService dbService;

    public DBControllerImpl() throws SQLException {
        dbService = new DBService();
    }

    @Override
    public void createTables() throws SQLException {
        dbService.createTables();
    }

    @Override
    public String createEquipmentAndWellIfNeed(String wellName, int countEquipment) {
       return dbService.createEquipmentAndWellIfNeed(wellName, countEquipment);
    }

    @Override
    public void close() throws SQLException {
        dbService.close();
    }

    @Override
    public List<Well> getNeededWells(String input) {
        return dbService.getNeededWells(input);
    }

    @Override
    public List<Well> getAllWellsWithEquipments() {
        return dbService.getAllWellsWithEquipments();
    }
}