package controller;

import service.EquipmentCreator;
import service.WellInfoGetter;
import service.XMLGetter;
import utils.Utils;

import javax.swing.*;
import java.awt.*;

public class UIControllerImpl extends Utils implements UIController {
    static JFrame jFrame = getFrame(3, "wellsworker");
    static JPanel jPanel = new JPanel();

    public UIControllerImpl() {
    }

    @Override
    public void selectVariants() {
        jFrame.add(jPanel);

        GridBagLayout gridBagLayout = new GridBagLayout();
        jPanel.setLayout(gridBagLayout);

        JButton btnAddWells = new JButton("Добавить оборудование");
        jPanel.add(btnAddWells, getGridBagConstraints(1, 1, 2, 6));

        JButton btnGetInfo = new JButton("Вывести информацию на экран");
        jPanel.add(btnGetInfo, getGridBagConstraints(1, 5, 2, 6));

        JButton btnGetInfoToXML = new JButton("Выгрузить информацию в файл");
        jPanel.add(btnGetInfoToXML, getGridBagConstraints(1, 10, 2, 6));

        btnAddWells.addActionListener(e -> {
            new EquipmentCreator();
            jPanel.revalidate();
        });
        btnGetInfo.addActionListener(e -> {
            new WellInfoGetter();
            jPanel.revalidate();
        });
        btnGetInfoToXML.addActionListener(e -> {
            new XMLGetter();
            jPanel.revalidate();
        });
        jPanel.revalidate();
    }
}
