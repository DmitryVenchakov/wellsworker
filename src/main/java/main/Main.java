package main;

import controller.DBController;
import controller.DBControllerImpl;
import controller.UIController;
import controller.UIControllerImpl;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {
        DBController dbController = new DBControllerImpl();
        dbController.createTables();
        UIController uiController = new UIControllerImpl();
        uiController.selectVariants();
        dbController.close();
    }
}
