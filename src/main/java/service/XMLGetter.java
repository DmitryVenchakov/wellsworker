package service;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.awt.*;
import java.io.File;
import java.sql.SQLException;
import java.util.List;

import controller.DBController;
import controller.DBControllerImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import pojo.Equipment;
import pojo.Well;
import utils.Utils;

public class XMLGetter extends Utils {
    static JFrame jFrame = getFrame(2, "Выгрузка информации в файл");
    static JPanel jPanel = new JPanel();

    public XMLGetter() {
        getInfoToXML();
    }

    private void getInfoToXML() {
        jFrame.add(jPanel);
        jPanel.add(new JLabel(),
                getGridBagConstraints(0,0,2,2));
        GridBagLayout gridBagLayout = new GridBagLayout();
        jPanel.setLayout(gridBagLayout);

        JTextField jTextField = new JTextField(30);
        jPanel.add(jTextField,
                getGridBagConstraints(0,0,3,2));
        JButton jButton = new JButton("Submit");
        jPanel.add(jButton, getGridBagConstraints(5, 5, 2, 2));

        jButton.addActionListener(e -> {
            jPanel.setVisible(true);
            DBController dbController = null;
            try {
                dbController = new DBControllerImpl();

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            List<Well> allWellsWithEquipments = dbController.getAllWellsWithEquipments();
            createToXMLFile(jTextField.getText(), allWellsWithEquipments);
            jPanel.revalidate();
        });

        jPanel.revalidate();
    }

    private void createToXMLFile(String fileName, List<Well> wells) {
        File file = new File(String.format("./%s.xml", fileName));
        DocumentBuilderFactory crunchifyDocBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = crunchifyDocBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element mainRootElement = doc.createElement("dbinfo");
            doc.appendChild(mainRootElement);
            wells.forEach(well -> mainRootElement.appendChild(getWell(doc, well)));
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);

            StreamResult streamResult = new StreamResult(file);
            transformer.transform(source, streamResult);
        } catch (TransformerException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private static Node getWell(Document doc, Well well) {
        Element wellElement = doc.createElement("well");
        wellElement.setAttribute("name", well.getName());
        wellElement.setAttribute("id", String.valueOf(well.getId()));
        well.getEquipmentList().forEach(equipment -> {
            wellElement.appendChild(getEquipmentsElements(doc, wellElement, equipment));
        });
        return wellElement;
    }

    private static Node getEquipmentsElements(Document doc, Element element, Equipment equipment) {
        Element equipmentElement = doc.createElement("equipment");
        equipmentElement.setAttribute("name", equipment.getName());
        equipmentElement.setAttribute("id", String.valueOf(equipment.getId()));
        element.appendChild(equipmentElement);
        return equipmentElement;
    }
}