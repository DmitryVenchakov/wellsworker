package service;

import org.sqlite.SQLiteConfig;
import pojo.Equipment;
import pojo.Well;
import utils.Utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBService extends Utils {

    private static final String DB_URL = "jdbc:sqlite:test.db";
    private static final String CREATE_TABLE_WELL = "CREATE TABLE IF NOT EXISTS well (\n" +
            "id INTEGER CONSTRAINT well_pk PRIMARY KEY AUTOINCREMENT,\n" +
            "name TEXT(32) NOT NULL UNIQUE);";
    private static final String CREATE_TABLE_EQUIPMENT = "CREATE TABLE IF NOT EXISTS equipment (\n" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "name TEXT(32) NOT NULL UNIQUE,\n" +
            "well_id INTEGER NOT NULL,\n" +
            "FOREIGN KEY(well_id) REFERENCES well(id)\n);";
    private static final String CREATE_WELL = "INSERT INTO well (name) VALUES (?);";
    private static final String CREATE_EQUIPMENT = "INSERT INTO equipment (name, well_id) VALUES (?, ?);";
    private static final String SELECT_WELL_ID_BY_NAME = "SELECT id FROM well\n" +
            "WHERE name = ?;";
    private static final String SELECT_COUNT_FROM_EQUIPMENT = "SELECT COUNT() FROM equipment;";
    private static final String DELETE_EQUIPMENT_BY_WELL_ID = "DELETE FROM equipment WHERE well_id = ?;";
    private static final String SELECT_ID_NAME_FROM_WELL = "SELECT id, name FROM well";
    private static final String SELECT_ID_NAME_FROM_EQUIPMENT = "SELECT id, name FROM equipment " +
            "WHERE well_id = ?;";

    private Connection connection;

    public DBService() throws SQLException {
        SQLiteConfig config = new SQLiteConfig();
        config.setPragma(SQLiteConfig.Pragma.FOREIGN_KEYS, "On");
        this.connection = DriverManager.getConnection(DB_URL, config.toProperties());
    }

    public void createTables() throws SQLException {
        try (Statement statement = this.connection.createStatement()) {
            statement.executeUpdate(CREATE_TABLE_WELL);
            statement.executeUpdate(CREATE_TABLE_EQUIPMENT);
        }
    }

    public String createEquipmentAndWellIfNeed(String wellName, int countEquipment) {
        String result;
        Integer wellId = getWellIdByName(wellName);
        if (wellId == null) {
            createWell(wellName);
            wellId = getWellIdByName(wellName);
            result = String.format("Создана скважина %s, количество оборудования %d", wellName, countEquipment);
        } else {
            result = String.format("На скважине %s обновлено количество оборудования %d", wellName, countEquipment);
        }
        deleteEquipmentsByWellId(wellId);
        createEquipment(generateUniqueNamesOfEquipmentList(countEquipment), wellId);
        return result;
    }

    public void close() throws SQLException {
        connection.close();
    }

    public List<Well> getNeededWells(String input) {
        List<Well> wellsList = getAllWellsWithEquipments();
        return Utils.getNeedWellsListFromAll(input, wellsList);
    }

    public List<Well> getAllWellsWithEquipments() {
        List<Well> wellsList = new ArrayList<>();
        try {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ID_NAME_FROM_WELL)) {
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    Well well = getWell(resultSet);
                    wellsList.add(well);
                }
            }
            try (PreparedStatement psEquipment = connection.prepareStatement(SELECT_ID_NAME_FROM_EQUIPMENT)) {
                int wellId;
                for (Well well : wellsList) {
                    wellId = well.getId();
                    psEquipment.setInt(1,wellId);
                    ResultSet resultSet = psEquipment.executeQuery();
                    List<Equipment> equipmentList = new ArrayList<>();
                    while (resultSet.next()) {
                        Equipment equipment = getEquipment(resultSet, wellId);
                        equipmentList.add(equipment);
                    }
                    well.setEquipmentList(equipmentList);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return wellsList;
    }

    private Well getWell(ResultSet resultSet) throws SQLException {
        return new Well(resultSet.getInt(1), resultSet.getString(2));
    }

    private Equipment getEquipment(ResultSet resultSet, int wellId) throws SQLException {
        return new Equipment(resultSet.getInt(1), resultSet.getString(2), wellId);
    }

    private void createWell(String wellName) {
        try {
            try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE_WELL)) {
                preparedStatement.setString(1, wellName);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void createEquipment(List<String> names, int wellId) {
        try {
            try (PreparedStatement preparedStatement = connection.prepareStatement(CREATE_EQUIPMENT)) {
                names.forEach(s -> {
                    try {
                        preparedStatement.setString(1, s);
                        preparedStatement.setInt(2, wellId);
                        preparedStatement.executeUpdate();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private Integer getWellIdByName(String wellName) {
        int resultFromQuery = getResultFromQuery(SELECT_WELL_ID_BY_NAME, wellName);
        return resultFromQuery != -1 ? resultFromQuery : null;
    }

    private int getResultFromQuery(String query, String wellName) {
        int result = -1;
        try {
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, wellName);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    result = resultSet.getInt(1);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result;
    }

    private int getCountOfEquipment() {
        int result = 0;
        try {
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_COUNT_FROM_EQUIPMENT)) {
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    result = resultSet.getInt(1);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result;
    }

    private void deleteEquipmentsByWellId(int wellId) {
        try{
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_EQUIPMENT_BY_WELL_ID)) {
                preparedStatement.setInt(1, wellId);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<String> generateUniqueNamesOfEquipmentList(int numberOfEq) {
        List<String> resultGeneration = new ArrayList<>();
        int countEquipment = getCountOfEquipment();
        for(int i = countEquipment + 1; i <= numberOfEq + countEquipment; i++) {
            resultGeneration.add(String.format("EQ%04d", i));
        }
        return resultGeneration;
    }
}