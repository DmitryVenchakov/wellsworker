package service;

import controller.DBController;
import controller.DBControllerImpl;
import pojo.Well;
import utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.List;

public class WellInfoGetter extends Utils {
    static JFrame jFrame = getFrame(2, "Вывод информации о выбранных скважинах на экран");
    static JPanel jPanel = new JPanel();

    public WellInfoGetter() {
        getInfo();
    }

    private void getInfo() {
        jFrame.add(jPanel);

        jPanel.add(new JLabel(),
                getGridBagConstraints(0,0,2,2));

        GridBagLayout gridBagLayout = new GridBagLayout();
        jPanel.setLayout(gridBagLayout);

        JTextField jTextField = new JTextField(30);
        jPanel.add(jTextField,
                getGridBagConstraints(0,0,3,2));
        JButton jButton = new JButton("Submit");
        jPanel.add(jButton, getGridBagConstraints(5, 5, 2, 2));

        jButton.addActionListener(e -> {
            List<Well> wellsList;
            jPanel.setVisible(true);
            String text = jTextField.getText();
            StringBuilder sb = new StringBuilder();
            DBController dbController = null;
            try {
                dbController = new DBControllerImpl();

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            wellsList = dbController.getNeededWells(text);
            wellsList.forEach(well -> {
                sb.append(well.getName())
                        .append(" - ")
                        .append(well.getEquipmentList().size())
                        .append("\n");
            });
            JTextArea area1 = new JTextArea(sb.toString(), 10, 6);
            jPanel.add(area1, getGridBagConstraints(0,10,25,25));
            jPanel.revalidate();
        });
        jPanel.revalidate();
    }
}