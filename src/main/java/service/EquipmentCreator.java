package service;

import controller.DBController;
import controller.DBControllerImpl;
import utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;

public class EquipmentCreator extends Utils {
    static JFrame jFrame = getFrame(2, "Добавление оборудования");
    static JPanel jPanel = new JPanel();

    public EquipmentCreator() {
        createEquipmentsOnWell();
    }

    private void createEquipmentsOnWell() {
        jFrame.add(jPanel);

        GridBagLayout gridBagLayout = new GridBagLayout();
        jPanel.setLayout(gridBagLayout);

        jPanel.add(new JLabel("Введите имя скважины"),
                getGridBagConstraints(0,0,2,2));

        JTextField jTextField1 = new JTextField(10);
        jPanel.add(jTextField1,
                getGridBagConstraints(4,0,3,2));

        jPanel.add(new JLabel("Введите количество оборудования"),
                getGridBagConstraints(0,3,2,2));

        JTextField jTextField2 = new JTextField(10);
        jPanel.add(jTextField2,
                getGridBagConstraints(4,3,3,2));

        JButton jButton = new JButton("Submit");
        jPanel.add(jButton, getGridBagConstraints(5, 5, 2, 2));

        jButton.addActionListener(e -> {
            jPanel.setVisible(true);
            int inputCount = Integer.parseInt(jTextField2.getText());
            String inputText = jTextField1.getText().toUpperCase();
            DBController dbController = null;
            try {
                dbController = new DBControllerImpl();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            String message = dbController.createEquipmentAndWellIfNeed(inputText, inputCount);
            jPanel.add(new JLabel(message),
                    getGridBagConstraints(0,-1,2,2));
            jPanel.show();
        });
        jPanel.revalidate();
    }
}