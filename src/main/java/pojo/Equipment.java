package pojo;

import java.util.Objects;

public class Equipment {

    private int id;
    private String name;
    private int well_id;

    public Equipment(int id, String name, int well_id) {
        this.id = id;
        this.name = name;
        this.well_id = well_id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getWell_id() {
        return well_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Equipment equipment = (Equipment) o;
        return id == equipment.id &&
                well_id == equipment.well_id &&
                Objects.equals(name, equipment.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, well_id);
    }
}
