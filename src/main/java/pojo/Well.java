package pojo;

import java.util.List;
import java.util.Objects;

public class Well {

    private int id;
    private String name;
    private List<Equipment> equipmentList;

    public Well(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Equipment> getEquipmentList() {
        return equipmentList;
    }

    public void setEquipmentList(List<Equipment> equipmentList) {
        this.equipmentList = equipmentList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Well well = (Well) o;
        return id == well.id &&
                Objects.equals(name, well.name) &&
                Objects.equals(equipmentList, well.equipmentList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, equipmentList);
    }
}
