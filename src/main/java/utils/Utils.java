package utils;

import pojo.Well;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class Utils {

    private static final List<String> SEPARATORS = asList(" ", ",");
    private static String separators;

    protected static List<Well> getNeedWellsListFromAll(String input, List<Well> wellsList) {
        separators = String.join("|\\", SEPARATORS);
        Set<String> setWellsName = Arrays
                .stream(input.split(separators))
                .map(String::trim)
                .collect(Collectors.toSet());
        return wellsList.stream()
                .filter(well -> setWellsName.contains(well.getName()))
                .collect(Collectors.toList());
    }

    protected static GridBagConstraints getGridBagConstraints(
            int gridx, int gridy, int gridheight, int gridwidth) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.weightx = 0;
        gridBagConstraints.weighty = 0;
        gridBagConstraints.gridx = gridx;
        gridBagConstraints.gridy = gridy;
        gridBagConstraints.gridheight = gridheight;
        gridBagConstraints.gridwidth = gridwidth;
        return gridBagConstraints;
    }

    protected static JFrame getFrame(Integer windowConstants, String frameName) {
        JFrame frame = new JFrame(frameName);
        frame.setVisible(true);
        frame.setBounds(50, 50, 600, 300);
        frame.setDefaultCloseOperation(windowConstants);
        return frame;
    }
}